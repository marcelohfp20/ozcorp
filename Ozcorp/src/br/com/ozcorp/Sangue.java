package br.com.ozcorp;
/**
 * 
 * @author Marcelo Henrique
 *
 */

public enum Sangue {
	AP("A+"), AN("A-"), BP("B+"), BN("B-"), ABP("AB+"), ABN("AB-"), OP("O+"), ON("O-");

	public String san;

	Sangue(String san) {
		this.san = san;
	}
}
