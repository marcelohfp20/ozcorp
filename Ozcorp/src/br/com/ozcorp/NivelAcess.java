package br.com.ozcorp;
/**
 * 
 * @author Marcelo Henrique
 *
 */

public enum NivelAcess {
	DIRETORIA("0"), SECRETARIA("1"), GERENTE("2"), ENGENHEIRO("3"), ANALISTA("4");

	public String s;

	NivelAcess(String s) {
		this.s = s;
	}
}
