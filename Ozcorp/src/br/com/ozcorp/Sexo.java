package br.com.ozcorp;
/**
 * 
 * @author Marcelo Henrique
 *
 */

public enum Sexo {
	MASCULINO("Masculino"), FEMININO("Feminino"), OUTROS("Outro");

	public String sex;

	Sexo(String sex) {
		this.sex = sex;
	}
}
